# DIY JBC Soldering Station

A Open Platform for C245 and other JBC soldering cartridges.
Here is a link to the [EasyEDA Project](https://easyeda.com/romanhayn99/jbc_smthng)

## Features

* High Efficiency design, achieved using a solid state relay made from low Resistance mosfets and using Zero Voltage Switching to pass full AC waves
* Fast Heating
* Fast Adjustment of Soldering Tempreture using a Rotary encoder as the only UI
* Sleep mode when resting in soldering iron holder. Temp drops to 150°C

## Design Files

You can find the BOM and gerber files in "pcb/"

## Source Code

Source code / Arduino code file is "JBCmini.ino"

There are certain parameters in the software, that need to be adjusted according to some Resistors on the pcb.